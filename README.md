# Todo CLI

Todo CLI is a simple program that uses a sqlite3 database to keep track of your tasks.

## Setup

You will need to modify your .bashrc with the following at the bottom of the file to make the program executable from anywhere in the terminal.

```bash
export PATH=$PATH:insert absolute file path here
```

Then save and exit from the .bashrc file and run the following command to update the path.

```bash
source ~/.bashrc
```

To check if it works just type `Todo.py` and the Todo help message should display.
