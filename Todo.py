#!/usr/bin/env python3

# By bigfiga

import sqlite3
import os
import sys 


###########################
######### Setup ###########
###########################

ARGS = sys.argv
loc = os.getenv("HOME")


# Checks if database is made if not makes it and builds the table.
def make_table():
    is_made = os.path.exists(loc +"/.tasks.db")

    if not is_made:
        conn = sqlite3.connect(loc + "/.tasks.db")
        c = conn.cursor()

        c.execute("""CREATE TABLE tasks (
            task text,
            status text
        )""")
make_table()

# Connect to the database.
conn = sqlite3.connect(loc + "/.tasks.db")
# Make a cursor. 
c = conn.cursor()


###########################
#### Command Functions ####
###########################

# Display the table.
def view():
    c.execute("SELECT rowid, * FROM tasks")
    items = c.fetchall()

    
    print("\nROW  TASK \t STATUS")
    print("-----------------------")
    for item in items:
        print(str(item[0]) + ":   " +  item[1] + "\t " + item[2])
    print("\n")

# Add task to the table.
def add_task():
    task = ARGS[2]
    c.execute("INSERT INTO tasks(task, status) VALUES ('{}', 'Not Done')".format(task))
    print("Added task")

# Change status to done.
def set_done(num):
    row = num
    c.execute("""UPDATE tasks SET status = 'Done'
                WHERE rowid = {}
            
    """.format(row))
    print("Task set to done")

# Delete a task from table
def del_task(num):
    row = num
    c.execute("DELETE FROM tasks WHERE rowid = {}".format(row))
    print("Task removed")

# Drops table and rebuilds it.
def clear():
    c.execute("DROP TABLE tasks")
    c.execute("""CREATE TABLE tasks (
            task text,
            status text
        )""")
    print("Tasks removed")


def info():
    print('''

    -----------------------------------------------
    | Welcome to the Todo CLI.                    |
    |                                             |
    | This program is a simple                    |
    | CLI to keep track of your tasks.            |
    |---------------------------------------------|
    | Commands        Description                 |
    |                                             |
    | blank           Displays this message       |
    | view            Shows all tasks             |
    | done            Sets a task as being done   |
    | add             Adds a task to the table    |
    | del             Deletes a task from table   |
    | clear           Clears all tasks from table |
    |---------------------------------------------|
    | The task must be surrounded by quotes ""    |
    -----------------------------------------------

    ''')


###########################
######## Parser ###########
###########################

if len(ARGS) == 1:
    info()
elif len(ARGS) > 3:
    print('The Task must be surrounded by ""')
elif ARGS[1] == "view":
    view()
elif ARGS[1] == "done":
    try:
        num = int(ARGS[2])
        set_done(num)
    except:
        print("Must be an interger")
elif ARGS[1] == "add":
    add_task()
elif ARGS[1] == "del":
    try:
        num = int(ARGS[2])
        del_task(num)
    except:
        print("Must be an interger")
elif ARGS[1] == "clear":
    clear()
else:
    print("Invalid argument")

###########################


# Save changes and close the database.
conn.commit()
conn.close()
